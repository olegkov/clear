package com.mmtr.spring.task22.service;

import com.mmtr.spring.task22.component.Dictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MyService implements IService {
    private static final Logger logger = LoggerFactory.getLogger(MyService.class);

    @Autowired
    private List<Dictionary> dictionaries;
    private Scanner scanner = new Scanner(System.in);
    private Dictionary currentDictionary;

    public MyService(){
    }

    public void showMenu() throws IOException {
        while(true){
            logger.info("1. Choose Dictionary to work");
            logger.info("2. Exit");

            int index = makeChoose();

            switch(index){
                case 1:
                    currentDictionary = chooseDictionary();
                    dictionaryActions();
                    break;
                case 2:
                    logger.info("System closing...");
                    System.exit(0);
                default:
                    break;
            }
        }
    }

    @Override
    public void showDictionaries() {
        for(int i = 0; i < dictionaries.size(); i++){
            logger.info("{}. {}", i, dictionaries.get(i).getName());
        }
    }

    @Override
    public void remove(String key) {
        currentDictionary.remove(key);
    }

    @Override
    public String search(String key) {
        return currentDictionary.search(key);
    }

    @Override
    public void add(String key, String value) {
        currentDictionary.add(key, value);
    }

    @Override
    public Dictionary chooseDictionary() {
        int index;
        while(true){
            logger.info("Choose dictionary: ");
            showDictionaries();
            index = makeChoose();
            if(index > dictionaries.size() - 1 || index < 0){
                logger.info("Incorrect number, try again");
                continue;
            }
            break;
        }

        return dictionaries.get(index);
    }

    @Override
    public void showRecords() {
        currentDictionary.getRecords().forEach((k, v) -> logger.info("{}: {}", k, v));
    }

    public void dictionaryActions() throws IOException {
        logger.info("Dictionary Actions:");
        logger.info("1. Show all");
        logger.info("2. Delete");
        logger.info("3. Search");
        logger.info("4. Add");
        logger.info("5. Main menu");

        int innerIndex = makeChoose();

        String key;
        String value;
        switch(innerIndex){
            case 1:
                showRecords();
                break;
            case 2:
                logger.info("Enter key for delete: ");
                key = scanner.next();
                remove(key);
                break;
            case 3:
                logger.info("Enter key for search: ");
                key = scanner.next();
                logger.info("Key: {}, value: {}", key, search(key));
                break;
            case 4:
                logger.info("Enter key: ");
                key = scanner.next();

                logger.info("Enter value: ");
                value = scanner.next();

                add(key, value);
                break;
            case 5:
                showMenu();
                break;
            default:
                break;
        }
    }

    private int makeChoose(){
        int index;

        while(true){
            try{
                index = scanner.nextInt();
                break;
            }catch (InputMismatchException e){
                logger.info("Please input numbers only");
                logger.info("Try again: ");
                scanner = new Scanner(System.in);
            }
        }

        return index;
    }
}
