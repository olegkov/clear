package com.mmtr.task1.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dictionary {
    private static final Logger logger = LoggerFactory.getLogger(Dictionary.class);
    private Map<String, String> records = new HashMap<>();
    private final String NAME;
    private final Pattern PATTERN;

    private Properties properties = new Properties();
    private File file;

    public Dictionary(File file, String regex){
        PATTERN = Pattern.compile(regex);
        NAME = file.getName();
        this.file = file;
        load(file);
        read();
    }

    private boolean load(File file) {
        try(InputStream in = new BufferedInputStream(new FileInputStream(file))){
            properties.load(in);
        }catch(IOException e){
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    private boolean store(){
        try(FileOutputStream fileOutputStream = new FileOutputStream(file)){
            properties.store(fileOutputStream, "DICTIONARY");
        }catch(IOException e){
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public boolean read(){
        try{
            for(String key : properties.stringPropertyNames()){
                records.put(key, new String(properties.getProperty(key).getBytes("ISO8859-1")));
            }
        }catch(Exception e){
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public boolean remove(String key){
        try{
            records.remove(key);
            properties.remove(key);
            store();
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public String search(String key){
        String value = records.get(key);
        return value != null ? value : "Record is missing!";
    }

    public boolean add(String rowKey, String value){
        try{
            Matcher matcher = PATTERN.matcher(rowKey);
            if(matcher.matches()){
                String key = matcher.group("key");
                records.put(key, value);
                properties.setProperty(key, value);
                store();
            }else{
                logger.info("Key '{}' dont match dictionary scheme", rowKey);
                return false;
            }
        }catch(Exception e){
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public String getName(){
        return NAME;
    }

    public Map<String, String> getRecords(){
        return records;
    }

    @Override
    protected void finalize() throws Throwable{
        try{
            store();
        }catch(Exception e){
            logger.error(e.getMessage(), e);
        }
    }
}
