package com.mmtr.task1.storage;

import com.mmtr.task1.component.Dictionary;

import java.util.ArrayList;
import java.util.List;

public interface IStorage {
    List<Dictionary> dictionaries = new ArrayList<>();

    void load();
    default List<Dictionary> getDictionaries(){
        return dictionaries;
    }
}
