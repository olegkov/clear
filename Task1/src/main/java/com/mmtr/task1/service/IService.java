package com.mmtr.task1.service;

import com.mmtr.task1.component.Dictionary;

public interface IService {
    void showDictionaries();
    void remove(String key);
    String search(String key);
    void add(String key, String value);
    Dictionary chooseDictionary();
    void showRecords();
}
